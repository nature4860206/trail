package com.thursdaypractice.thursdaypractice.controller;



import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/hello")
public class ThursdayPracticeController {
	@GetMapping
	public String welcome(){
		return "WELCOME";
		
	}
	
	@GetMapping("/testrequestparam")
	public String getRequestparam(@RequestParam String id) {
		return "Id using requestParam = " + id;
		
	}
	@GetMapping("/testpathvariable/{id}")
	public String getPathVariable(@PathVariable("id") String id) {
		return "Id using pathvariable = " + id;
	}
	@PostMapping("/empty")
	public String emptyBody() {
		return "Empty request body";
	}
}
